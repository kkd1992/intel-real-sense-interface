#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include "ObjectDet.hpp"
#define MAX_THRESH 255
using namespace cv;

ObjectDet::ObjectDet(unsigned int gThresh,double minA)
{
    grayThreshold=gThresh;
    minArea=minA;
}


// countour detection with colour image input
cv::Mat ObjectDet::algo1(const cv::Mat frame)
{

    cv::Mat src=frame.clone();

    //std::vector<Mat> channels;
    // split(src,channels);
    cv::Mat src_gray;
    /// Convert the image to grayscale if the image is colored
    if (frame.channels() == 3)
    {
    cvtColor( src, src_gray, CV_BGR2GRAY );
    }
    else {src_gray=src.clone();}
    namedWindow( "gray image",CV_WINDOW_NORMAL);
    imshow( "gray image", src_gray);

    ///conver to black and white using threshold
    cv::Mat threshold_output;

    threshold( src_gray, threshold_output, grayThreshold, MAX_THRESH, THRESH_BINARY );

    namedWindow( "threshold_output",CV_WINDOW_NORMAL);
    imshow( "threshold_output", threshold_output );

    /// Detect edges
    std::vector<std::vector<Point> > contours;
    std::vector<Vec4i> hierarchy;

    findContours( threshold_output, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0) );

    /// Approximate contours to polygons + get bounding rects and circles
    std::vector<std::vector<Point> > contours_poly( contours.size() );
    std::vector<std::vector<Point> >hull( contours.size() );
    std::vector<Rect> boundRect( contours.size() );
    std::vector<Point2f>center( contours.size() );
    std::vector<float>radius( contours.size() );


    int I=0,j=0;
    for( size_t i = 0; i < contours.size(); i++ )
    {
        approxPolyDP( Mat(contours[i]), contours_poly[i], 3, true );
        convexHull( Mat(contours[i]), hull[i], false );
        // area=contourArea(contours_poly[i]);
        area=contourArea(hull[i]);
        if (area>minArea)
        {
            boundRect[I] = boundingRect( Mat(contours_poly[i]) );
            I++;
        }
    }

    /// Draw polygonal contour + bonding rects + circles
    cv::Mat dst = Mat::zeros( src.size(), CV_8UC3 );
    for (j=0;j<=I;j++)
    {
    //for( size_t j = 0; j< contours.size(); j++ )//for convex hulls
        {
            rectangle( dst, boundRect[j].tl(), boundRect[j].br(), color, 2, 8, 0 );
        }
    }
    return dst.clone();
}

