#ifndef MAIN_HPP_INCLUDED
#define MAIN_HPP_INCLUDED

    #include "opencv2/core.hpp"
    #include "opencv2/features2d.hpp"
    #include "opencv2/highgui.hpp"
    #include <opencv2/calib3d.hpp>
    #include <opencv2/imgproc.hpp>
    #include <opencv2/objdetect.hpp>
    #include <opencv2/video.hpp>
    #include <opencv2/videoio.hpp>
    #include <opencv2/imgcodecs.hpp>
    #include <opencv2/bgsegm.hpp>
    #include <opencv2/photo.hpp>

    #include <stdio.h>
    #include <string.h>
    #include <ctype.h>
    #include <iostream>
    #include <fstream>
    #include <sstream>
    #include <stdlib.h>
    #include <ctime>
    #include <cmath>
    #include <iomanip>
    #include <thread>


    //#include "/home/anrc/image_processing/code/resize_frame.h"
    #include<sys/time.h>
    #include<unistd.h>

    #include <librealsense/rs.hpp>
    #include "example.hpp"

    #include "SOIL.h"

    #include "ObjectDet.hpp"

    using namespace cv;
    using namespace std;
    using namespace ogl;

    ObjectDet objdet(10,10);



#endif // MAIN_HPP_INCLUDED
