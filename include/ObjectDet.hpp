#ifndef OBJECTDET_HPP_INCLUDED
#define OBJECTDET_HPP_INCLUDED

		using namespace cv;
		using namespace std;
		class ObjectDet
		{
		public:
            ObjectDet(unsigned int gThresh,
                                double minA);
			cv::Mat algo1(const cv::Mat frame);
			void set_parameters(
                                );

        private:
            unsigned int grayThreshold=10;
            double area,maxArea,minArea=100;
            Scalar color = Scalar( 0,0,255 );
		};
#endif
